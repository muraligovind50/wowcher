package test;

import com.govind.InvalidTimeFormatException;
import com.govind.TimeConverter;

import java.util.ArrayList;

public class TestTime {


    public static void main(String[] args) {

        TimeConverter timeConverterObj= new TimeConverter();
        ArrayList<String> arrayList= new ArrayList<String>();
        try {
            arrayList.add(timeConverterObj.covertTimeToWords("08:34"));
            arrayList.add(timeConverterObj.covertTimeToWords("00:00"));
            arrayList.add(timeConverterObj.covertTimeToWords("12:00"));
            arrayList.add(timeConverterObj.covertTimeToWords("09:20"));
            arrayList.add(timeConverterObj.covertTimeToWords("00:12"));
            arrayList.add(timeConverterObj.covertTimeToWords("01:45"));

            for (String str : arrayList) {
                System.out.println(str);
            }


        } catch (InvalidTimeFormatException e) {
            System.out.println(e.getMessage());
        }


    }

}
