package com.govind;

public class InvalidTimeFormatException extends Exception{
    public InvalidTimeFormatException(String exp) {
        super(exp);
    }
}
