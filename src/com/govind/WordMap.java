package com.govind;

import java.util.HashMap;

public class WordMap {
    private static final HashMap<String,String> map=new HashMap<String,String>();

    static {
        map.put("00","Zero");
        map.put("01","One");
        map.put("02","Two");
        map.put("03","Three");
        map.put("04","Four");
        map.put("05","Five");
        map.put("06","Six");
        map.put("07","Seven");
        map.put("08","Eight");
        map.put("09","Nine");
        map.put("10","Ten");
        map.put("11","Eleven");
        map.put("12","Twelve");
        map.put("13","Thirteen");
        map.put("14","Fourteen");
        map.put("15","Fifteen");
        map.put("16","Sixteen");
        map.put("17","Seventeen");
        map.put("18","Eighteen");
        map.put("19","Nineteen");
        map.put("20","Twenty");
        map.put("30","Thirty");
        map.put("40","Forty");
        map.put("50","Fifty");
    }

    public static String getValue(String strObj){
        return map.get(strObj);
    }
}
