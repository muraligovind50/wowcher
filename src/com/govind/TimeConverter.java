package com.govind;

public class TimeConverter {

    public String covertTimeToWords(String strTime) throws InvalidTimeFormatException {

        String[] strArray= strTime.split(":");
        StringBuilder stringBuilder = new StringBuilder("The time is ");
        if(!strTime.matches("^[0-9]{2}:[0-9]{2}$") || Integer.parseInt(strArray[0])>23  || Integer.parseInt(strArray[1])>59){
            throw new InvalidTimeFormatException("Invalid Time Format");
        }

        if(strArray[0].equals("00")  && strArray[1].equals("00")){
            stringBuilder.append("at Midnight!");
        }else if(strArray[0].equals("12") && strArray[1].equals("00")){
            stringBuilder.append("at Midday!");
        }else {
            if(WordMap.getValue(strArray[0])==null){
                stringBuilder.append(getWordsOf(strArray[0]));
                stringBuilder.append(" hours and ");
            }else{
                stringBuilder.append(WordMap.getValue(strArray[0]));
                stringBuilder.append(" hours and ");
            }
            if(WordMap.getValue(strArray[1])==null){
                stringBuilder.append(getWordsOf(strArray[1]));
                stringBuilder.append(" minutes ");
            }else{
                stringBuilder.append(WordMap.getValue(strArray[1]));
                stringBuilder.append(" minutes ");
            }

        }
       return stringBuilder.toString();

    }
    private  String getWordsOf( String str){
        String strTens=str.substring(0,1).concat("0");
        String strOnes="0".concat(String.valueOf(str.charAt(1)));
        return WordMap.getValue(strTens)+" "+WordMap.getValue(strOnes);
    }

}
